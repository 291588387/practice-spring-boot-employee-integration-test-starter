package com.afs.restapi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    MockMvc client;
    @Autowired
    EmployeeController employeeController;

    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp() {
        employeeRepository.clearAll();
    }

    @Test
    void should_get_all_employees_when_get_all_employees() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 5000);
        employeeRepository.insert(employee);
        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(5000));
    }

    @Test
    void should_return_employee_when_get_employee_by_id_given_id() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 5000);
        Employee savedEmployee = employeeRepository.insert(employee);
        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/employees/{id}", savedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(5000));
    }

    @Test
    void should_create_employee_when_create_employee_given_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 5000);
        String employeeJson = "{\n" +
                "\"name\": \"Lucy\",\n" +
                "\"age\": 20,\n" +
                "\"gender\": \"female\",\n" +
                "\"salary\": 5000\n" +
                "}";
        //when
        //then
        client.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(5000));
    }

    @Test
    void should_update_employee_when_update_employee_given_id_and_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 5000);
        employeeRepository.insert(employee);
        String updatedEmployee = "{\n" +
                "\"age\": 21,\n" +
                "\"salary\": 8000\n" +
                "}\n";
        //when
        //then
        client.perform(MockMvcRequestBuilders.put("/employees/{id}", employee.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployee))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(employee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(21))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(8000));
    }

    @Test
    void should_delete_employee_when_delete_employee_given_id() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Lucy1", 20, "female", 5000);
        Employee employee2 = new Employee(2L, "Lucy2", 20, "female", 5000);
        employeeRepository.insert(employee1);
        employeeRepository.insert(employee2);
        Long idToBeDeleted = employee2.getId();
        //when
        //then
        client.perform(MockMvcRequestBuilders.delete("/employees/{id}", idToBeDeleted))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
        assertEquals(1, employeeRepository.getEmployees().size());
    }

    @Test
    void should_return_employees_list_when_findEmployeesByGender_given_gender() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Lucy1", 20, "female", 5000);
        Employee employee2 = new Employee(2L, "Lucy2", 20, "female", 5000);
        Employee employee3 = new Employee(3L, "Lucy3", 20, "male", 5000);
        employeeRepository.insert(employee1);
        employeeRepository.insert(employee2);
        employeeRepository.insert(employee3);
        String targetGender = "female";
        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/employees?gender={gender}", targetGender))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(2)));
    }

    @Test
    void should_return_employees_list_when_findByPage_given_page_and_size() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Lucy1", 20, "female", 5000);
        Employee employee2 = new Employee(2L, "Lucy2", 20, "female", 5000);
        Employee employee3 = new Employee(3L, "Lucy3", 20, "male", 5000);
        Employee employee4 = new Employee(4L, "Lucy4", 20, "male", 5000);
        employeeRepository.insert(employee1);
        employeeRepository.insert(employee2);
        employeeRepository.insert(employee3);
        employeeRepository.insert(employee4);
        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/employees?page={page}&size={size}", 2,3))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(employee4.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employee4.getName()));
    }
}
